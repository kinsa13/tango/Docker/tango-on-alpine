# TANGO Controls on Alpine

This Dockerfile is based on the work done here: https://gitlab.com/ska-telescope/ska-tango-images

```
docker build --build-arg GIT_REV="$(git rev-parse HEAD)" --build-arg BUILD_DATE="$(date)" -t <tag>
```
