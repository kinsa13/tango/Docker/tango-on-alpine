ARG BASE_IMAGE="alpine:3.15"
FROM $BASE_IMAGE as buildenv

ARG LIBZMQ_VER=v4.3.4
ARG CPPZMQ_VER=v4.8.1
ARG OMNIORB_VER=4.2.4

# Install build time dependencies
RUN apk --update add --no-cache bash curl \
    openssl ca-certificates libstdc++ \
    g++ make libsodium-dev git cmake python3-dev
    
# build and install libzmq
RUN git clone -b ${LIBZMQ_VER} --depth 1 https://github.com/zeromq/libzmq /libzmq && \
    cmake -B /libzmq/build -DCMAKE_INSTALL_PREFIX=/usr/local -DENABLE_DRAFTS=OFF -DWITH_DOC=OFF -DZMQ_BUILD_TESTS=OFF /libzmq  && \
    make  -j$(nproc) -C /libzmq/build install

# build and install cpp zmq
RUN git clone -b ${CPPZMQ_VER} --depth 1 https://github.com/zeromq/cppzmq.git /cppzmq && \
    cmake -B /cppzmq/build -DCPPZMQ_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX=/usr/local /cppzmq && \
    make  -j$(nproc) -C /cppzmq/build install
    
# build and install omniORB
RUN mkdir /omniORB && \
    curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-${OMNIORB_VER}/omniORB-${OMNIORB_VER}.tar.bz2/download -o - | \
            tar xjf - --strip-components=1 -C /omniORB && \
    cd /omniORB && \
    ./configure --prefix=/usr/local/ && \
    make  -j$(nproc) && \
    make install

RUN curl -L https://github.com/vishnubob/wait-for-it/raw/master/wait-for-it.sh -o /usr/local/bin/wait-for-it && \
    chmod a+x /usr/local/bin/wait-for-it

# Install build time dependencies
RUN apk add mariadb-connector-c-dev

# build and install tangoidl
RUN git clone --depth 1 https://gitlab.com/tango-controls/tango-idl.git /idl && \
    cmake -B /idl/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /idl && \
    make  -j$(nproc) -C /idl/build install

ARG TANGO_VER=9.3.5

## build and install cppTango
RUN git clone -b ${TANGO_VER} --depth=1 https://gitlab.com/tango-controls/cppTango.git /cppTango && \
    cd /cppTango && \
    sed -i '1s/^/#include <sys\/types.h>\n#include <bits\/alltypes.h>\n/' /cppTango/log4tango/include/log4tango/FileAppender.hh && \
    mkdir build && \
    cmake . -B build \
      -DBUILD_TESTING=OFF \
      -DCPPZMQ_BASE=/usr/local/ \
      -DIDL_BASE=/usr/local/ \
      -DOMNI_BASE=/usr/local/ \
      -DZMQ_BASE=/usr/local/ && \
    make  -j$(nproc) -C build && \
    make  -C build install

# build and install tango_admin
RUN git clone --depth=1 https://gitlab.com/tango-controls/tango_admin.git /tango_admin && \
    cmake -B /tango_admin/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /tango_admin && \
    make  -j$(nproc) -C /tango_admin/build install

# build and install TangoDatabase
RUN git clone --depth 1 https://gitlab.com/tango-controls/TangoDatabase.git /TangoDatabase && \
    cmake -B /TangoDatabase/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /TangoDatabase && \
    make  -j$(nproc) -C /TangoDatabase/build install && \
    mv /usr/local/bin/Databaseds /usr/local/bin/DataBaseds

# build and install TangoTest
RUN git clone --depth 1 https://gitlab.com/tango-controls/TangoTest /src/tangotest && \
    cmake -B /src/tangotest/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /src/tangotest && \
    make  -j$(nproc) -C /src/tangotest/build install

# build and install SerialLine
RUN git clone --depth 1 https://gitlab.com/kinsa13/tango/DeviceServers/SerialLine.git /src/serial && \
    cmake -B /src/serial/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /src/serial && \
    make  -j$(nproc) -C /src/serial/build install

# build and install Modbus
RUN git clone --depth 1 https://gitlab.com/kinsa13/tango/DeviceServers/Modbus.git /src/modbus && \
    cmake -B /src/modbus/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /src/modbus && \
    make  -j$(nproc) -C /src/modbus/build install

# build and install ModbusComposer
RUN git clone --depth 1 https://gitlab.com/kinsa13/tango/DeviceServers/ModbusComposer.git /src/modbuscomposer && \
    cmake -B /src/modbuscomposer/build -DCMAKE_INSTALL_PREFIX=/usr/local/ /src/modbuscomposer && \
    make  -j$(nproc) -C /src/modbuscomposer/build install

FROM $BASE_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="tango-on-alpine" \
    org.opencontainers.image.description="TANGO Controls built using Alpine Linux (based on https://gitlab.com/ska-telescope/ska-tango-images)" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/tango-on-alpine" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/tango-on-alpine" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

COPY --from=buildenv /usr/local /usr/local

RUN apk --update add --no-cache libstdc++ bash libsodium mariadb-connector-c && \
    adduser -h /home/tango -s /bin/bash -D tango

USER tango
